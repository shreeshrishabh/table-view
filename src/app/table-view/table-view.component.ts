import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Tableview } from './table.model';
import { data } from './data';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.css']
})
export class TableViewComponent implements OnInit {

  displayedColumns: string[] = ['date', 'listName', 'noOfEntities', 'action', 'details'];
  dataSource: MatTableDataSource<Tableview>;
  selectedTable: Tableview = new Tableview();
  data = data;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor() {
    this.dataSource = new MatTableDataSource(data);
    // console.log(this.dataSource.data);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getDetails(row) {
    this.selectedTable = this.data.filter(a => a.id === row.id)[0];
  }

}
